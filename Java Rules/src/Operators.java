public class Operators {
    public static void main(String[] args) {
        Integer numero = 10;

        System.out.println(numero + 950);
        System.out.println(numero - 89);
        System.out.println(numero * 2);
        numero *= 2;
        System.out.println(numero / 3);
        numero += 30;
        System.out.println(numero % 3);

        //Utiliza a variável e depois incrementa
        numero++;
        //numero += 1;
        //numero = numero + 1;

        //Incrementa antes de utilizar a variável;
        ++numero;


        numero--;
        //numero -= 1;
        //numero = numero - 1;

    }
}
