public class Exceptions {
    public static void main(String[] args) {
        try {
            String str = "Curso de Java";
            str.charAt(200);

            String s = null;
            System.out.println(s.length());

        }catch (StringIndexOutOfBoundsException e){
            System.out.println("Out of Bounds!");
        }catch (NullPointerException e){
            System.out.println("NullPointer!");
        }catch (Exception e){
            System.out.println("Erro Desconhecido!");
        }finally {
            System.out.println("Executei!");
        }
        System.out.println("Teste!!!!");
    }
}
