import java.util.Scanner;

public class UserInfo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Informe uma string:");

        String str = scanner.nextLine();

        System.out.println(str);

        Integer valor;
        System.out.print("Informe um numero:");

        valor = scanner.nextInt();
        System.out.println(valor + 10);
        /*
            Float ft = scanner.nextFloat();
            Double db = scanner.nextDouble();
            Long lg = scanner.nextLong();
            Short st = scanner.nextShort();
            Byte bt = scanner.nextByte();
            Boolean bl = scanner.nextBoolean();
            Integer it = scanner.nextInt();
        */
    }
}
