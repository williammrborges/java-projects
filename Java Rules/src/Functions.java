import java.util.Locale;

public class Functions {
    private void helloWorld(){
        System.out.println("Olá Mundo!");
    }

    private long soma(long a, long b){
        return a+b;
    }

    public static void main(String[] args) {
        Functions funcoes = new Functions();
        funcoes.helloWorld();

        System.out.println(funcoes.soma(2, 6));

        String str = "Curso de Java";

        System.out.println(str.equals("Java"));

        System.out.println(str.startsWith("Java"));

        System.out.println(str.endsWith("Java"));

        System.out.println(str.substring(3));
        System.out.println(str.substring(3,5));

        System.out.println(str.replace("Java", "Javaaaaaa"));

        System.out.println(str.toUpperCase());
        System.out.println(str.toLowerCase());
        System.out.println(str.trim());
        System.out.println(str.length());
    }
}
